#include <simple_qt_gui/mainwindow.hpp>

namespace simple_qt_gui
{

MainWindow::MainWindow(QWidget *parent) :
        QMainWindow(parent)
{
  QApplication::setWindowIcon(QIcon("://simple_qt_gui.png"));
  parent = new QWidget;
  setCentralWidget(parent);
  QHBoxLayout *layout(new QHBoxLayout);
  parent->setLayout(layout);
  layout->addWidget(new QLabel("This is a Qt application that can make use of ROS"));

  setWindowTitle("ROS Qt application");
}

MainWindow::~MainWindow()
{
}

}
