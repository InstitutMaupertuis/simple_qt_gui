#include <simple_qt_gui/mainwindow.hpp>
#include <QApplication>

int main(int argc,
         char *argv[])
{
  ros::init(argc, argv, "simple_qt_gui");

  QApplication a(argc, argv);
  simple_qt_gui::MainWindow w;
  w.setWindowTitle("Qt GUI with ROS");
  w.show();
  return a.exec();
}
