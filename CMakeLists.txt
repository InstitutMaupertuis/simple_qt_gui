cmake_minimum_required(VERSION 3.2)
project(simple_qt_gui)
add_compile_options(-Wall -Wextra)
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

## Find catkin macros and libraries
find_package(catkin REQUIRED COMPONENTS
  roscpp
)

## Find Qt5
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
find_package(Qt5Widgets REQUIRED)

################################################
## Declare ROS messages, services and actions ##
################################################

################################################
## Declare ROS dynamic reconfigure parameters ##
################################################

###################################
## catkin specific configuration ##
###################################

catkin_package(
  INCLUDE_DIRS
  include
  CATKIN_DEPENDS
  roscpp
  DEPENDS
  Qt5Widgets
)

###########
## Build ##
###########

include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

# GUI
add_executable(
  ${PROJECT_NAME}_gui
  desktop/resources.qrc
  include/${PROJECT_NAME}/mainwindow.hpp
  src/main.cpp
  src/mainwindow.cpp
)
target_link_libraries(
  ${PROJECT_NAME}_gui
  ${catkin_LIBRARIES}
  Qt5::Widgets
)

#############
## Install ##
#############

if("${CMAKE_INSTALL_PREFIX}" MATCHES "^/opt/")
  # Handle release case
  set(DESKTOP_APP_DIR "/usr/share/applications")
  set(DESKTOP_ICON_DIR "/usr/share/icons")
else()
  set(DESKTOP_APP_DIR "$ENV{HOME}/.local/share/applications")
  set(DESKTOP_ICON_DIR ${DESKTOP_APP_DIR})
  set(DESKTOP_APP_SUFFIX " (local)")
endif()

# Create directory for the desktop file/icon
install(DIRECTORY DESTINATION
  ${DESKTOP_APP_DIR}
)
install(DIRECTORY DESTINATION
  ${DESKTOP_ICON_DIR}
)

# Configure bash scripts
configure_file(
  scripts/${PROJECT_NAME}.bash.in
  scripts/${PROJECT_NAME}.bash
  @ONLY
)

# Install bash scripts
install(
  PROGRAMS
  ${CMAKE_CURRENT_BINARY_DIR}/scripts/${PROJECT_NAME}.bash
  DESTINATION
  ${CATKIN_PACKAGE_BIN_DESTINATION}
)

# Configure / install desktop launcher
configure_file(
  desktop/${PROJECT_NAME}.desktop.in
  desktop/${PROJECT_NAME}.desktop
)
install(
  FILES
  ${CMAKE_CURRENT_BINARY_DIR}/desktop/${PROJECT_NAME}.desktop
  DESTINATION
  ${DESKTOP_APP_DIR}
)

# Install icon
install(
  FILES
  desktop/${PROJECT_NAME}.png
  DESTINATION
  ${DESKTOP_ICON_DIR}
)

## Mark executables and/or libraries for installation
# Qt panel
install(
  TARGETS
  ${PROJECT_NAME}_gui
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

## Mark cpp header files for installation
install(
  DIRECTORY
  include/${PROJECT_NAME}/
  DESTINATION
  ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
)

## Mark other files for installation (e.g. launch and bag files, etc.)
install(
  DIRECTORY
  launch
  DESTINATION
  ${CATKIN_PACKAGE_SHARE_DESTINATION}
)

#############
## Testing ##
#############
